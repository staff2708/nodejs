import axios from 'axios';
const serverUrl = 'http://localhost:4000';

export async function getDevices() {
    const res = await axios.get(`${serverUrl}/devices`);
    return res.data;
}

export async function getDeviceById(deviceId) {
    const res =  await axios.get(`${serverUrl}/devices/${deviceId}`);
    return res.data;
}

export async function addDevice(device) {
    console.log(device);
    await axios.post(`${serverUrl}/devices`, device)
}

export async function removeDevice(deviceId) {
    await axios.delete(`${serverUrl}/devices/${deviceId}`);
}

export async function updateDevice(deviceId, data) {
    await axios.patch(`${serverUrl}/devices/${deviceId}`,data);
}

export async function switchOn(deviceId) {
    await axios.patch(`${serverUrl}/devices/activate/${deviceId}`);
}

export async function switchOff(deviceId) {
    await axios.patch(`${serverUrl}/devices/deactivate/${deviceId}`);
}

export async function getDeviceLog(deviceId) {
    const res = await axios.get(`${serverUrl}/devices/log/${deviceId}`);
    return res.data;
}