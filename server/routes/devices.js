const express = require('express');
const devicesService = require('../services/devices');
const request = require('http').request;
const router = express.Router();

router.get('/', async (req, res) => {
    const devices = await devicesService.getDevices();
    res.json(devices);
})

router.get('/:id', async (req, res) => {
    const { id } = req.params;
    const device = await devicesService.getDeviceById(id);
    if(device) {
        res.json(device);
    } else {
        res.sendStatus(404)
    }
})

router.post('/', async (req, res) => {
    const { name, address, port } = req.body;
    await devicesService.addDevice({ name, address, port});
    res.sendStatus(201);
});

router.delete('/:id', async (req, res) => {
    const { id } = req.params;
    await devicesService.removeDevice(id);
    res.sendStatus(200);
})

router.patch('/:id', async (req, res) => {
    const { id } = req.params;
    console.log(req.body);
    const { name, address, port, state } = req.body;
    await devicesService.updateDevice(id, { name, address, port, state})
    res.sendStatus(200);
});

router.patch('/activate/:id', async (req, res) => {
    const { id } = req.params;
    await devicesService.switchOn(id);
    res.sendStatus(200);
})

router.patch('/deactivate/:id', async (req, res) => {
    const { id } = req.params;
    await devicesService.switchOff(id);
    res.sendStatus(200);
})

router.get('/log/:id', async (req, res) => {
    const { id } = req.params;
    const logs = await devicesService.getDeviceLog(id);
    res.json(logs);
});

module.exports = router;