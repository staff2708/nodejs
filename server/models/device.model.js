const mongoose = require('mongoose');

const device = new mongoose.Schema({
    name: String,
    address: String,
    port: Number,
    state: String
});

const DeviceModel = mongoose.model('Device', device);

module.exports = DeviceModel;