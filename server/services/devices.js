const DeviceModel = require('../models/device.model');
const { sendRequest } = require('../utils/request');

function deviceAdapter({ name, address, port, state, _id}) {
    return {
        id: _id,
        name,
        address,
        port,
        state
    }
}


async function getDevices() {
    const devices = await DeviceModel.find({}).exec();
    return devices.map(deviceAdapter);
}

async function getDeviceById(deviceId) {
    const device = await DeviceModel.findById(deviceId).exec();
    return deviceAdapter(device);
}

async function addDevice(device) {
    const dev = new DeviceModel({
        state: 'off',
        ...device
    });

    await dev.save();
}

async function removeDevice(deviceId) {
    await DeviceModel.findByIdAndDelete(deviceId);
}

// async function updateDevice(deviceId, data) {
//     await DeviceModel.updateOne({_id: deviceId }, {name: data.name, address: data.address, port: data.port, state: data.state}).exec();
// }

async function switchOn(deviceId) {
    await updateDevice(deviceId, {
        state: 'on'
    });
}

async function switchOff(deviceId) {
    await updateDevice(deviceId, {
        state: 'off'
    });
}

async function getDeviceLog(deviceId) {
    return [
        {
            date: '2018-31-08 16:00:00',
            action: 'On'
        },
        {
            date: '2018-31-08 17:00:00',
            action: 'Off'
        }]
}

async function updateDevice(deviceId, data) {
    const device = await DeviceModel.findById(deviceId).exec();

    if (!device) {
        return null;
    }

    if(data.state){
        await updateDeviceState(
            device.address,
            device.port,
            data.state
        )
    }

    await DeviceModel.findByIdAndUpdate(deviceId, data).exec();
}

async function updateDeviceState(address, port, state) {
    const command = state === 'off' ? 'Power off' : 'Power On';
    const url = `http://${address}:${port}/cm?cmnd=${command}`;
    await sendRequest(url);
}

module.exports = {
    getDevices: getDevices,
    getDeviceById: getDeviceById,
    addDevice: addDevice,
    removeDevice: removeDevice,
    updateDevice: updateDevice,
    switchOn: switchOn,
    switchOff: switchOff,
    getDeviceLog: getDeviceLog,
};