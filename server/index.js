const express = require('express');
const app = express();
const mongoose = require('mongoose');

const corsMiddleware = require('./middlewares/cors');
const devicesRouter = require('./routes/devices'); 

app.use(corsMiddleware);
app.use(express.json());

app.use('/devices', devicesRouter);

app.get('/', (req, res) => {
    res.send({'status': 200})
})

app.listen(4000, () => {
    console.log('Server listening on port 4000');
    mongoose.connect('mongodb://localhost:27017/node-workshop-4', { useNewUrlParser: true})
})